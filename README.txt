Team: java_chips
Author: Jiazhen Cao, Kaijing Zhang, Weigang An
Project: submarine attack

Art Works: Weigang An
Animation: Kaijing Zhang
Class structure: Jiazhen Cao
Programming and debugging: Jiazhen Cao, Weigang An, Kaijing Zhang

--------------------
How To Build and Run
--------------------
Makefile is available, so simplely use 'make' command to build a main.exe.
(Make sure you have sfml environment available when make)
Than use './main.exe' to run the game.

----------------
Game Instruction
----------------
During the game: Press "SPACE" to attack submarines
				 Press "LEFT" or "RIGHT" to move

Just like the project name, this game is to attack and survive.

*** Submarines will spawn from left and right with different speed, they might
	or might not attack you during appearing on the screen.

*** If you hit an enemy, you get 50 pts, it you get hit, you loose 1 life.

*** You will reach higher LEVEL as your score gets higher.

*** The highest LEVEL we set is LEVEL 10, if you finished LEVEL 10, you win!

*** If you loose all 3 life, then game is over.

*** More and more submarines will spawn as the LEVEL increases.

HAVE FUN! o(*￣▽￣*)ブ