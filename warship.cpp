#include "warship.hpp"
#include "entity.hpp"
Warship::Warship() {
	name = "warship";
}

void Warship::update() {
	if (move) {
		x += dx;
	}
}

void Warship::setdx(int change) {
	dx = change;
}

float Warship::getx() {
	return x;
}

float Warship::gety() {
	return y;
}