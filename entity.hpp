#ifndef ENTITY_HPP_
#define ENTITY_HPP_
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Network.hpp>
#include <string>
#include "explosion.hpp"

class Entity {
public:
	sf::Sprite sprite;
	float x, y, dx, dy;
	int w, h, d, app_width, app_height;
	int life;
	std::string name;
	Explosion boom;

	Entity();

	void setting(sf::Texture &t, float X, float Y, int W, int H, int direction, int wW, int wH);

	void ExplosionSetting(Explosion &e, float X, float Y);

	virtual void update();

	void draw(sf::RenderWindow &w);

	virtual ~Entity();
};
#endif