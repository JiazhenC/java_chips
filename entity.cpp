#include "entity.hpp"
#include "explosion.hpp"
Entity::Entity() {
	life = 1;
}

void Entity::setting(sf::Texture &t, float X, float Y, int W, int H, int direction, int wW, int wH) {
	x = X;
	y = Y;
	d = direction;
	w = W;
	h = H;
	app_width = wW;
	app_height = wH;
	if (d == 1) {
		sprite.setTexture(t);
		sprite.setTextureRect(sf::IntRect(w, 0, -w, h));
	}
	else {
		sprite.setTexture(t);
	}
	sprite.setOrigin(w / 2, h / 2);
}

void Entity::ExplosionSetting(Explosion &e, float X, float Y) {
	boom = e;
	x = X;
	y = Y;
}

void Entity::update() {};

void Entity::draw(sf::RenderWindow &w) {
	sprite.setPosition(x, y);
	boom.sprite.setPosition(x, y);
	w.draw(sprite);
	w.draw(boom.sprite);
}

Entity::~Entity() {};