#ifndef EXPLOSION_HPP_
#define EXPLOSION_HPP_
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Network.hpp>
class Explosion {
public:
	float Frame, speed;
	sf::Sprite sprite;
	std::vector<sf::IntRect> frame;

	Explosion();

	Explosion(sf::Texture &t, int x, int y, int W, int H, int fps, float s);

	void update();

	bool End();
};

#endif