#ifndef WARSHIP_HPP_
#define WARSHIP_HPP_
#include "entity.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

class Warship : public Entity {
public:
	bool move;
	Warship();

	void update();

	void setdx(int change);

	float getx();

	float gety();

};
#endif