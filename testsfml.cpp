#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Network.hpp>
#include <iostream>
#include <time.h>
#include <list>
#include <string>
#include "entity.hpp"
#include "explosion.hpp"
#include "warship.hpp"
#include "submarine.hpp"
#include "missile.hpp"
const int windowW = 1100;
const int windowH = 447;
int LEVEL = 1;
int Score = 0;
bool restart = false;
bool start_game = true;

bool isCollide(Entity *ship, Entity *miss) {
	if ((ship->y - ship->h/2 <= miss->y)
		&& (ship->y + ship->h/2 >= miss->y) 
		&& (ship->x - ship->w/2 <= miss->x) 
		&& (ship->x + ship->w/2 >= miss->x)) {
		return true;
	}
	return false;
}

void start() {
	srand(time(0));
	sf::RenderWindow window(sf::VideoMode(windowW, windowH), "Bombard Submarines");
	sf::Texture pTexture, backtexture, eTexture, mTexture, m1Texture, exTexture, e2Texture;
	sf::Sprite background;
	sf::Font font;
	sf::Text title, mytext, stext, chance, end, s, instruction, pause_m;
	title.setCharacterSize(100);
	title.setFillColor(sf::Color::Black);
	title.setFont(font);
	title.setString("submarine attack");
	s.setFillColor(sf::Color::Black);
	s.setFont(font);
	s.setString("press enter to start game");
	instruction.setFillColor(sf::Color::Black);
	instruction.setFont(font);
	instruction.setString("(Space to Attack, left and right to move)");
	mytext.setPosition(windowW - 200, 0);
	mytext.setFont( font );
	mytext.setFillColor(sf::Color::Black);
	stext.setPosition(windowW - 200, 25);
	stext.setFont( font );
	stext.setFillColor(sf::Color::Black);
	chance.setFont( font );
	chance.setPosition(0,0);
	chance.setFillColor(sf::Color::Black);
	pause_m.setFont(font);
	pause_m.setFillColor(sf::Color::Black);
	pause_m.setString("Press Enter to Continue");
	end.setFont( font );
	end.setFillColor(sf::Color::Black);
	end.setCharacterSize( 50 );

	std::list<Entity*> elements;

	if (!pTexture.loadFromFile("pictures/warship.png"))
		std::cout << "Error could not load warship image" << std::endl;
	if (!backtexture.loadFromFile("pictures/background.jpg"))
		std::cout << "Error could not load background image" << std::endl;
	if (!eTexture.loadFromFile("pictures/submarines.png", sf::IntRect(0, 70, 156, 45)))
		std::cout << "Error could not load submarines.png" << std::endl;
	if (!e2Texture.loadFromFile("pictures/submarines.png", sf::IntRect(0, 0, 96, 65)))
		std::cout << "Error could not load second submarines.png" << std::endl;
	if (!mTexture.loadFromFile("pictures/warship_missile.png"))
		std::cout << "Error could not load warship missile image" << std::endl;
	if (!m1Texture.loadFromFile("pictures/submarine_missile.png"))
		std::cout << "Error could not load submarine missile image" << std::endl;
	if (!exTexture.loadFromFile("pictures/boom.png"))
		std::cout << "Error could not load boom image" << std::endl;
	if (!font.loadFromFile("Capture.ttf"))
		std::cout << "Error loading file" << std::endl;

	background.setTexture(backtexture);
	Explosion explode(exTexture, 0, 0, 192, 192, 64, 0.5);

	Warship *warship = new Warship();
	warship->setting(pTexture, windowW/2, 120, 163, 52, 0, windowW, windowH);
	elements.push_back(warship);
	warship->life = 3;

	int enemy_count = 0;
	bool delay = false;
	std::string LIVE = "Life : OOO\npress esc to pause";
	mytext.setString("LEVEL 1");
	stext.setString("Score 0");
	chance.setString(LIVE);
	while (window.isOpen()) {
		sf::Event event;
		window.draw(background);
		if (start_game == true) {
			float t_w = title.getLocalBounds().width;
			float t_h = title.getLocalBounds().height;;
			title.setPosition(windowW / 2 - t_w / 2, windowH / 2.0 - 2 * t_h);
			float s_w = s.getLocalBounds().width;
			s.setPosition(windowW / 2 - s_w / 2, windowH / 2.0);
			float i_w = instruction.getLocalBounds().width;
			float i_h = instruction.getLocalBounds().height;
			instruction.setPosition(windowW / 2 - i_w / 2, windowH / 2.0 + 2 * i_h);
			window.draw(title);
			window.draw(s);
			window.draw(instruction);
			window.display();
		}
		while (start_game) {
			window.clear();
			while (window.pollEvent(event)) {
				if (event.type == sf::Event::KeyPressed) {
					if (event.key.code == sf::Keyboard::Return) {
						start_game = false;
					}
				}
				if (event.type == sf::Event::Closed) {
					restart = false;
					start_game = false;
					window.close();
				}
			}
		}
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				restart = false;
				window.close();
			}
			if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Space && delay == false) {
					Missile *missile = new Missile();
					missile->setting(mTexture, warship->x, warship->y + warship->h/2, 7, 20, 0, windowW, windowH);
					elements.push_back(missile);
					missile->setdy(0.25);
					missile->name = "warship_missile";
				}
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
			bool pause = true;
			float p_w = pause_m.getLocalBounds().width;
			pause_m.setPosition(windowW / 2 - p_w / 2, windowH / 2.0);
			window.draw(pause_m);
			window.display();
			while (pause) {
				while (window.pollEvent(event)) {
					if (event.type == sf::Event::Closed) {
						restart = false;
						window.close();
					}
					if (event.type == sf::Event::KeyPressed) {
						if (event.key.code == sf::Keyboard::Return) {
							pause = false;
						}
					}
				}
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) and warship->getx() < (windowW - 80)) {
			warship->move = true;
			warship->setdx(1);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) and warship->getx() > 80) {
			warship->move = true;
			warship->setdx(-1);
		}
		else {
			warship->move = false;
		}

		if (enemy_count < LEVEL){
			Submarine *s = new Submarine();
			int d = rand() % 2;
			int sub = rand() % 2 + 1;
			float speed = 0.15 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.6 - 0.15)));
			if (sub == 2){
				if( d == 0){
					s->setting(e2Texture, -(rand() % 200 * LEVEL + 100), rand() % (windowH - 245) + 200, 96, 65, 0, windowW, windowH);
					s->setdx(speed);
				}				
				else{
					s->setting(e2Texture, rand() % 200 * LEVEL + (windowW + 100), rand() % (windowH - 245) + 200, 96, 65, 1, windowW, windowH);
					s->setdx(-speed);
				}
			}
			else{
				if (d == 0) {
					s->setting(eTexture, -(rand() % 200 * LEVEL + 100), rand() % (windowH - 245) + 200, 150, 42, 0, windowW, windowH);
					s->setdx(speed);
			}
				else {
					s->setting(eTexture, rand() % 200 * LEVEL + (windowW + 100), rand() % (windowH - 245) + 200, 150, 42, 1, windowW, windowH);
					s->setdx(-speed);
				}
			}
			s->name = "submarine";
			elements.push_back(s);
			enemy_count++;
		}

		for (auto n : elements) {	
			int fashe = rand() % 10000;
			if (n->name == "submarine") {
				if ((5 <= fashe) && (fashe <= 7) && (n->x > n->w) && (n->x < windowW - n->w)) {
					Missile *missile = new Missile();
					missile->setting(m1Texture, n->x, n->y, 5, 17, 1, windowW, windowH);
					elements.push_back(missile);
					missile->setdy(-0.25);
					missile->name = "submarine_missile";
				}		
			}
		}
		
		for (auto s : elements) {
			for (auto m : elements) {
				if ((s->name == "submarine") && (m->name == "warship_missile")){
					if (isCollide(s, m)) {
						s->life = 0;
						m->life = 0;
						Entity *e = new Entity();
						e->ExplosionSetting(explode, s->x, s->y);
						e->name = "explosion";
						elements.push_back(e);
						Score += 50;
						//std::cout << "Score: " << Score << std::endl;
						std::string s1 = std::to_string(Score);
						std::string s2 = "Score " + s1;
						stext.setString(s2);
					}
				} else if ((s->name == "warship") && (m->name == "submarine_missile")){
					if (isCollide(s, m)) {
						s->life--;
						if (s->life == 2) LIVE = "life : OO\npress esc to pause";
						if (s->life == 1) LIVE = "life : O\npress esc to pause";
						if (s->life == 0) LIVE = "life : \npress esc to pause";
						chance.setString(LIVE);
						m->life = 0;
						Entity *e = new Entity();
						e->ExplosionSetting(explode, s->x, s->y);
						e->name = "explosion";
						elements.push_back(e);
					}
				}
			}
		}
		
		for(auto e : elements) {
			if (e->name == "explosion") {
				if (e->boom.End()) {
					e->life = 0;
				}
			}
		}
		delay = false;
		for (auto i = elements.begin(); i != elements.end(); ) {
			Entity *e = *i;
			e->update();
			e->boom.update();
			if (e->name == "warship_missile" && e->y >= warship->y && e->y <= 250) delay =true;
			if (e->life == 0) {
				if (e->name == "submarine") {
					enemy_count--;
				}
				i = elements.erase(i);
				delete e;
			}
			else {
				i++;
			}
		}
		if (Score == 50 * LEVEL * LEVEL){
			if (LEVEL + 1 != 11) 
				LEVEL += 1;
			//std::cout << "LEVEL " << LEVEL - 1 << " Finished" << std::endl;
			std::string ss1 = std::to_string(LEVEL);
			std::string ss2 = "LEVEL " + ss1;
			mytext.setString(ss2);
		}
		for (auto e : elements) {
			e->draw(window);
		}
		window.draw(mytext);
		window.draw(stext);
		window.draw(chance);
		if (Score >= 50 * LEVEL * LEVEL || warship->life == 0) {
			std::string ss;
			if (Score == 50 * LEVEL * LEVEL) {
				ss = "win!!!";
				end.setString(ss);
			} else if (warship->life == 0) {
				ss = "GAME OVER!!!";
				end.setString(ss);
			}
			float end_w = end.getLocalBounds().width;
			end.setPosition(windowW / 2 - end_w / 2, windowH / 2.0);
			ss = ss + "\nPress Enter to Restart\nPress ESC to Close";
			end.setString(ss);
			window.draw(end);
			window.display();
			bool breaks = true;
			while (breaks) {
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
					breaks = false;
					restart = true;
					warship->life = 3;
					std::string LIVE = "Life : OOO";
					mytext.setString("LEVEL 1");
					stext.setString("Score 0");
					chance.setString(LIVE);
					LEVEL = 1;
					Score = 0;
					if (warship->life == 0) {
						elements.clear();
						Warship *warship = new Warship();
						warship->setting(pTexture, windowW / 2, 120, 163, 52, 0, windowW, windowH);
						elements.push_back(warship);
					}
					window.clear();
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
					breaks = false;
					restart = false;
				}
				while (window.pollEvent(event)) {
					if (event.type == sf::Event::Closed) {
						breaks = false;
						restart = false;
					}
				}
			}
			break;
		}
		window.display();
	}
}

int main(void) {
	do {
		start();
	} while(restart);

	return 0;
}
