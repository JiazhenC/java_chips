#ifndef SUBMARINE_HPP_
#define SUBMARINE_HPP_
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include "entity.hpp"

class Submarine : public Entity {
public:
	Submarine();

	void update();
	
	void setdx(float change);
};
#endif