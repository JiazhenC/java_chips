#include "missile.hpp"
#include "entity.hpp"

Missile::Missile() {}

void Missile::update() {
	y += dy;

	if ((y > app_height) || (y < 0)) {
		life = 0;
	}
}

void Missile::setdy(float change) {
	dy = change;
}
