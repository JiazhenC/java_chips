#include "explosion.hpp"

Explosion::Explosion() {}

Explosion::Explosion(sf::Texture &t, int x, int y, int W, int H, int fps, float s) {
	Frame = 0;
	speed = s;
	for (int i = 0; i < fps; i++) {
		frame.push_back(sf::IntRect(x+i*W, y, W, H));
	}
	sprite.setTexture(t);
	sprite.setOrigin(W / 2, H / 2);
	sprite.setTextureRect(frame[0]);
}

void Explosion::update() {
	Frame += speed;
	int n = frame.size();
	if (Frame >= n) Frame -= n;
	if (n > 0) sprite.setTextureRect(frame[int(Frame)]);
}

bool Explosion::End() {
	return Frame+speed>=frame.size();
}