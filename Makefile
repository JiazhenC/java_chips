CXX = g++
CFLAGS = -std=c++14 -g -Wall

%.o : %.cpp
	$(CXX) -c $(CFLAGS) $< 

# Add any additional targets you may have here 

# Link with external main (on ix-dev only)
main.exe: testsfml.o entity.o explosion.o submarine.o warship.o missile.o 
	$(CXX) -o $@ $^ -lsfml-graphics -lsfml-window -lsfml-audio -lsfml-network -lsfml-system

memcheck: main.exe
	valgrind --leak-check=yes --track-origins=yes main.exe

clean:
	$(RM) *.o *.exe
