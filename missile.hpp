#ifndef MISSILE_HPP_
#define MISSILE_HPP_
#include "entity.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

class Missile : public Entity {
public:
	Missile();

	void update();

	void setdy(float change);
};

#endif